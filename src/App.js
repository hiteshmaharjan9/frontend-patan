import "./App.css";
import Address from "./practiceComponent/Address";
import Age from "./practiceComponent/Age";
import College from "./practiceComponent/College";
import Detail from "./practiceComponent/Detail";
import Details1 from "./practiceComponent/Details1";
import Testing from "./practiceComponent/Testing";
import LearnTernary from "./practiceComponent/LearnTernary";
import EffectOnDifferentData from "./practiceComponent/EffectOnDifferentData";
import LearnMap1 from "./practiceComponent/LearnMap1";
import Info from "./practiceComponent/Info";
import LearnMap2 from "./practiceComponent/LearnMap2";
import AssignmentDay4 from "./practiceComponent/AssignmentDay4";
import ButtonClick from "./practiceComponent/ButtonClick";
import LearnUseStateHook1 from "./practiceComponent/LearnUseStateHook/LearnUseStateHook1";
import LearnUseStateHook2 from "./practiceComponent/LearnUseStateHook/LearnUseStateHook2";
import LearnUseStateHook3 from "./practiceComponent/LearnUseStateHook/LearnUseStateHook3";
import LearnUseStateHook4 from "./practiceComponent/LearnUseStateHook/LearnUseStateHook4";
import Name from "./practiceComponent/Name";
import Toggle from "./practiceComponent/LearnUseStateHook/Toggle";
import WhyUseState from "./practiceComponent/LearnUseStateHook/WhyUseState";
import Increment from "./practiceComponent/LearnUseStateHook/Increment";
import LearnUseEffectHook from "./practiceComponent/LearnUseEffectHook/LearnUseEffectHook";
import LearnCleanUpFunction from "./practiceComponent/LearnUseEffectHook/LearnCleanUpFunction";
import { createContext, useState } from "react";
import MyRoutes from "./practiceComponent/MyRoutes";
import MyLinks from "./practiceComponent/MyLinks";
import Form1 from "./practiceComponent/Form/Form1";
import Form2 from "./practiceComponent/Form/Form2";
import NestingRoute from "./practiceComponent/LearnRouting/NestingRoute";
import NestingRouteAssignment from "./practiceComponent/LearnRouting/NestingRouteAssignment";
import Form3 from "./practiceComponent/Form/Form3";
import Learn1UseRefHook from "./practiceComponent/learnUserRefHook/Learn1UseRefHook";
import AddDataToLocalStorage from "./practiceComponent/LearnLocalStorage/AddDataToLocalStorage";
import GetLocalStorage from "./practiceComponent/LearnLocalStorage/GetLocalStorage";
import RemoveDataLocalStorage from "./practiceComponent/LearnLocalStorage/RemoveDataLocalStorage";
import AddDataSessionStorage from "./practiceComponent/LearnSessionStorage/AddDataSessionStorage";
import GetDataSessionStorage from "./practiceComponent/LearnSessionStorage/GetDataSessionStorage";
import RemoveDataSessionStorage from "./practiceComponent/LearnSessionStorage/RemoveDataSessionStorage";
import Parent from "./practiceComponent/LearnPropDrilling/Parent";

export let Context1 = createContext();
export let Context2 = createContext();

function App() {
  // let name="Hitesh";
  let [name, setName] = useState("Hitesh");
  let [age, setAge] = useState(23);
  let [address, setAddress] = useState("Lalitpur");
  // setName("Hitesh");
  // setAge(23);
  return (
    <Context1.Provider
      value={{ name: name, age: age, setName: setName, setAge: setAge }}
    >
      <Context2.Provider value={{address: address, setAddress: setAddress}}>
        <Parent></Parent>
      </Context2.Provider>
    </Context1.Provider>
  );
}

// function App() {
//   // let a = <p style={{fontFamily:"serif"}}>This is a paragraph</p>;
//   // let b = "This is me";
//   // console.log(b);
//   // let v = <Name></Name>;
//   // let show = (e) => {
//   //   v = <Name></Name>
//   // }

//   // let hide = (e) => {
//   //   v = null;
//   // };

//   // let [showComp, setShowComp] = useState(true);

//   // let changeComp = () => {
//   //   let a;
//   //   if (showComp === true)
//   //     a = false;
//   //   else
//   //     a = true;
//   //   return ((e) => {
//   //     setShowComp(a);
//   //   });
//   // };

//   return (
//     // <div>
//     //   {/* <h1 style={{color: "blue", fontFamily:"cursive"}}>Heading 1</h1>
//     //   {a}
//     //   <p style={{backgroundColor: "red", color: "yellow"}}>Hello</p>
//     //   <span>Hitesh</span><span>Maharjan</span>
//     //   <a href="https://theverge.com" target=" ">The Verge</a>
//     //   <img src="./logo192.png" alt="file"></img>
//     //   <span>Hello</span>
//     //   <p className="success">this is me</p> */}
//     //   {/*
//     //   <Age></Age>
//     //   <Address></Address>
//     //   <Detail name="Hitesh" address="Lalitpur" age={29} arr={[1,2,3,4]}></Detail>
//     //   <Testing val1="success" val2="info"></Testing> */}
//     //   {/* <Details1 name="Hitesh" age={23} address="Lalitpur" style={{backgroundColor: "pink"}}></Details1>
//     //   <College name="Patan Multiple Campus" location="Patan Dhoka, Lalitpur"></College> */}
//     //   {/* <LearnTernary marks={85}></LearnTernary> */}
//     //   {/* <EffectOnDifferentData></EffectOnDifferentData> */}
//     //   {/* <LearnTernary age={15}></LearnTernary> */}
//     //   {/* <Info name="Hitesh" age={23} fatherDetail={{name: "heera", age:23}} favFood={["mutton", "yomari"]}></Info> */}
//     //   {/* <LearnMap1></LearnMap1> */}
//     //   {/* <LearnMap2></LearnMap2> */}
//     //   {/* <AssignmentDay4></AssignmentDay4> */}
//     //   {/* <ButtonClick></ButtonClick> */}
//     //   {/* <LearnUseStateHook1></LearnUseStateHook1> */}
//     //   {/* <LearnUseStateHook2></LearnUseStateHook2> */}
//     //   {/* <LearnUseStateHook3></LearnUseStateHook3> */}
//     //   {/* <LearnUseStateHook4></LearnUseStateHook4> */}
//     // </div>
//     <div>
//       {/* <Toggle></Toggle> */}
//       {/* <WhyUseState></WhyUseState> */}
//       {/* <Increment></Increment> */}
//       {/* <LearnUseEffectHook></LearnUseEffectHook> */}

//       {/* {showComp?<LearnCleanUpFunction></LearnCleanUpFunction>:null}
//       <br></br>
//       <button onClick={changeComp()}>{showComp?"hide":"show"}</button> */}
//       {/* <MyLinks></MyLinks>
//       <MyRoutes></MyRoutes> */}
//       {/* <Form1></Form1> */}
//       {/* <Form2></Form2> */}
//       {/* <MyRoutes></MyRoutes> */}
//       {/* <NestingRoute></NestingRoute> */}
//       {/* <NestingRouteAssignment></NestingRouteAssignment> */}
//       {/* <Form3></Form3> */}
//       {/* <Learn1UseRefHook></Learn1UseRefHook> */}
//       {/* <AddDataToLocalStorage></AddDataToLocalStorage> */}
//       {/* <GetLocalStorage></GetLocalStorage> */}
//       {/* <RemoveDataLocalStorage></RemoveDataLocalStorage> */}
//       {/* <AddDataSessionStorage></AddDataSessionStorage> */}
//       {/* <GetDataSessionStorage></GetDataSessionStorage> */}
//       {/* <RemoveDataSessionStorage/> */}
//       <Parent name="hitesh"></Parent>
//     </div>
//     )
// }

export default App;

// Alt + Shift + O to remove unused
