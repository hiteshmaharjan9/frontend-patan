import React, { useState } from "react";
import { days, genders } from "./Data";

const Form3 = () => {
  let [day, setDay] = useState("day1");
  let [gender, setGender] = useState("female");
  let onSubmit = (e) => {
    e.preventDefault();
    let data = {
      day: day,
      gender: gender,
    };
    console.log(data);
  };

  let optionDays = days.map((day, i) => {
    return (
      <option value={day.value} key={i + 1}>
        {day.label}
      </option>
    );
  });

  // let optionGenders = genders.map((gen, i) => {
  //   return (<option value={gen.value} key={i + 1}>{gen.label}</option>)
  // });

  let checked = (genderValue) => {
    return gender === genderValue;
  };

  let optionGenders = genders.map((gen, i) => {
    return (
      <div key={i}>
        <label htmlFor={gen.value}>{gen.label}</label>
        <input
          type="radio"
          id={gen.value}
          value={gen.value}
          checked={gender === gen.value}
          onChange={(e) => {
            setGender(e.target.value);
            console.log(e.target.value);
          }}
        ></input>
      </div>
    );
  });

  return (
    <div>
      <form onSubmit={onSubmit}>
        <label htmlFor="day">Day: </label>
        <select
          id="day"
          value={day}
          onChange={(e) => {
            setDay(e.target.value);
            console.log(e.target.value);
          }}
        >
          {optionDays}
        </select>
        <br></br>
        <div>Gender</div>
        {/* <label htmlFor="male">Male</label>
            <input type="radio" id="male" value="male" checked={gender === "male"} onChange={(e) => {
              setGender(e.target.value);
            }}></input>

            <label htmlFor="female">Female</label>
            <input type="radio" id="female" value="female" checked={gender === "female"} onChange={(e) => {
              setGender(e.target.value);
            }}></input>

            <label htmlFor="other">Other</label>
            <input type="radio" id="other" value="other" checked={gender === "other"} onChange={(e) => {
              setGender(e.target.value);
            }}></input> */}
        {optionGenders}

        {/* <label>Gender: </label>
            <select
              value={gender}
              onChange={(e) => {
                  setGender(e.target.value);
                  console.log(e.target.value);
              }}
            >
              {/*option*/}
        {/* {optionGenders} */}
        {/* </select> */}
        <br></br>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default Form3;
