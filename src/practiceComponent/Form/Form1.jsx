import React, { useState } from 'react'

const Form1 = () => {
    let [name, setName] = useState("");
    let [surname, setSurname] = useState("");
    let [email, setEmail] = useState("");
    let [password, setPassword] = useState("");
    let [phone, setPhone] = useState("");
    let [dob, setDob] = useState("");
    let [description, setDescription] = useState("");
    let onSubmit = (e) => {
        e.preventDefault();
        let data = {
            name: name,
            surname: surname,
            email: email,
            password: password,
            phone: phone,
            dob: dob,
            description: description
        };
        console.log(data);
        let [year, day, month] = dob.split("-");
        console.log(`Month = ${month}\nDay = ${day}\nYear = ${year}`);
    };
  return (
    <div>
        <form onSubmit={onSubmit}>
            <div>
                <label htmlFor="name">Name: </label>
                <input type="text" placeholder="Hitesh Maharjan" id="name" value={name} onChange={(e) => {
                    console.log("changed");
                    setName(e.target.value);
                    console.log(e.target.value);
                }}></input>
            </div>
            {/* in input whatever you place in value is displayed in the input in the browser */}
            <div>
                <label htmlFor="surname">Surname: </label>
                <input 
                type="text" 
                id="surname" 
                placeholder="Maharjan" 
                value={surname} 
                onChange={(e)=> {
                    console.log("surname");
                    setSurname(e.target.value);

                }}></input>
            </div>
            {/*email*/}
            <div>
                <label htmlFor="email">email: </label>
                <input 
                type="email" 
                placeholder="hitesh@gmail.com" 
                id="email" 
                value={email} 
                onChange={(e) => {
                    setEmail(e.target.value);
                    console.log(e.target.value);
                }}></input>
            </div>
            {/*password*/}
            <div>
                <label htmlFor="password">Password: </label>
                <input type="password" id="password" placeholder="your password*" value={password} onChange={(e) => {
                    setPassword(e.target.value);
                    console.log(e.target.value);
                }}></input>
            </div>
            
            <div>
                <label htmlFor="phone">Phone No.: </label>
                <input 
                type="tel" 
                id="phone" 
                placeholder="+977 - **********" 
                value={phone} 
                onChange={
                    (e) => {
                            setPhone(e.target.value);
                            console.log(e.target.value);
                            }
                        }>        
                </input>
            </div>

            <div>
                <label htmlFor="description">Description: </label>
                <textarea id="description" value={description} placeholder="description" onChange={
                    (e) => {
                        setDescription(e.target.value);
                        console.log(e.target.value);
                    }
                }></textarea>
            </div>

            <div>
                <label htmlFor="dob">Date of Birth</label>
                <input id="dob" type="date" value={dob} onChange={
                    (e) => {
                        setDob(e.target.value);
                        console.log(e.target.value);
                }}></input>
            </div>

            <br></br>
            <button type="submit">Proceed</button>
        </form>
    </div>
  )
}

export default Form1

//phone number => number
//dob => date