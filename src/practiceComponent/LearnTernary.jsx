import React from 'react'

const LearnTernary = ({marks, age}) => {
  let obj = {
    name: "hitesh",
    sec: "mern"
  };
  // return (
  //   <div>
  //     {
  //       (age < 18)?<div>underage</div>:
  //       (age < 60)?<div>adult</div>:
  //       <div>old</div>
  //     }
  //   </div>
  // )

    // return (
    //   <div>
    //     {
    //       (marks <= 39)?<div>Fail</div>:
    //       (marks <= 59)?<div>Third Division</div>:
    //       (marks <= 79)?<div>First Division</div>:
    //       (marks <= 100)?<div>Distinction</div>:
    //       null
    //     }        
    //   </div>
    // );

    return (
    <div>
      {
        (age < 18)?<div>Cannot enter bar</div>:
        <div>Can enter bar</div>
      }
    </div>
    );
};

export default LearnTernary;