import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ReadSpecificStudent = () => {
  let params = useParams();
  let [student, setStudent] = useState({});
  // console.log(params);

  let sendRequest = async () => {
    try {
          let result = await axios({
            url: `http://localhost:8000/students/${params.id}`,
            method: `get`
          });
          setStudent(result.data.result);
    } 
    catch (error) 
    {
      console.log(error);
      toast.error(error.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
    }
  }

  useEffect(() => {
    sendRequest();
  }, []);

  // console.log(student);

  let displayStudent = () => {
    return (<div>
      <p>Name: {student.name}</p>
      <p>Age: {student.age}</p>
      <p>isMarried: {String(student.isMarried)}</p>
    </div>)
  };



  return (
    <div>
      <ToastContainer></ToastContainer>
      {displayStudent()}
    </div>
  )
}

export default ReadSpecificStudent