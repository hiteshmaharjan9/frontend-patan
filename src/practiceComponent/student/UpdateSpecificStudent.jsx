import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { displayError, displaySuccess } from '../../toast';

const UpdateSpecificStudent = () => {
  let params = useParams();
  // console.log(params);
  // let name;
  let navigate = useNavigate();
  let [name, setName] = useState("");
  let [age, setAge] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  console.log(name);

  let getStudentData = async() => {
    try {
          let result = await axios({
            url: `http://localhost:8000/students/${params.id}`,
            method: `get`
          });
          result = result.data.result;
          setName(result.name);
          setAge(result.age);
          setIsMarried(result.isMarried);
    } catch (error) {
      console.log(error.message);
    }
  };


  useEffect(() => {
    getStudentData();

  }, []);



  let onSubmit = async(e) => {
    e.preventDefault();
    // console.log("name=>", name);
    console.log(typeof age);
    let data = {
      name: name,
      age: Number(age),
      isMarried: isMarried
    };
    console.log(data);

    try {
      let result = await axios({
        url: `http://localhost:8000/students/${params.id}`,
        method: `patch`,
        data: data
      });
      console.log(result);
      displaySuccess(result.data.message);
      // navigate(`/students/${params.id}`);
    }
    catch(error)
    {
      // console.log(error);
      displayError(error.message);

    }
  }

  return (
    <div>
      <ToastContainer/>
      <form onSubmit={onSubmit}>

        <label htmlFor="name">Name: </label>
        <input 
          type="text" 
          placeholder="John Doe" 
          id="name" 
          value={name} 
          onChange={(e) => {
            // name = e.target.value;
            setName(e.target.value);
            // console.log(e.target.value);
            // console.log(name);
          }
        }></input> 

        <br></br>

        <label htmlFor="age">Age: </label>
        <input 
          type="number" 
          placeholder="eg: 18" 
          id="age" 
          value={age} 
          onChange={(e) => {
            setAge(e.target.value);
          // console.log(age);
          }
        }></input> 

        <br></br>

        <label htmlFor="isMarried">isMarried: </label>
        <input 
          type="checkbox" 
          id="isMarried" 
          checked={isMarried === true} 
          onChange={(e) => {
            setIsMarried(e.target.checked);
            // console.log(e.target.checked);
          }
        }></input>

        <br></br>
        
        <button type="submit">update</button>
      
      </form>
    </div>
  )
}

export default UpdateSpecificStudent