/*
Create Student
With field name(text), age(number), isMarried(checkbox)
*/

import axios from 'axios';
import React, { useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const CreateStudent = () => {
  let [name, setName] = useState("");
  let [age, setAge] = useState("");
  let [isMarried, setIsMarried] = useState(false);

  let onSubmit = async(e) => {
    e.preventDefault();
    let data = {
      name: name,
      age: Number(age),
      isMarried: isMarried
    };
    // console.log(typeof data.age);
    // console.log(typeof data.isMarried);
    setName("");
    setAge("");
    setIsMarried(false);  
    console.log(data);
    try{
      let result = await axios({
        url: `http://localhost:8000/students`,
        method: "POST",
        data: data
      });
      console.log(result);
      toast.success(result.data.message, {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
    }
    catch (error)
    {
      console.log(error);
      toast.error(error.response.data.message, {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
    }

  };
  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>    
        <label htmlFor="name">Name: </label>
        <input type="text" placeholder="John Doe" id="name" value={name} onChange={(e) => {setName(e.target.value); console.log(e.target.value)}}></input>
        <br></br>
        <label htmlFor="age">Age: </label>
        <input type="number" placeholder="eg: 18" id="age" value={age} onChange={(e) => {setAge(e.target.value); console.log(e.target.value)}}></input>
        <br></br>
        <label htmlFor="isMarried">isMarried</label>
        <input type="checkbox" id="isMarried" checked = {isMarried === true} onChange={(e) => {setIsMarried(e.target.checked); console.log(e.target.checked)}}></input>
        <br></br>
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}

export default CreateStudent;




