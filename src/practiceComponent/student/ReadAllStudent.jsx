import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';



export const ReadAllStudent = () => {
  let [students, setStudents] = useState([]);
  let navigate = useNavigate();
  // let students = [];

  let sendRequest = async () => {
    let result = await axios({
      url: `http://localhost:8000/students`,
      method: `get`
    });
    // console.log(result);
    // students = result.data.result;
    setStudents(result.data.result);
  }

  let deleteStudent = (student) => {
     return (() => {
      let _deleteStudent = async() => {
        try {
          let result = await axios({
            url: `http://localhost:8000/students/${student._id}`,
            method: `delete`
          });
          console.log(result.data.message);
          sendRequest();
        } catch (error) {
          console.log(error.message);
        }
      };
      _deleteStudent();
  });
}

  useEffect(() => {
    sendRequest();
  }, []);

  console.log(students);
  let displayStudents = () => {
    let out = students.map((student, i) => {
      console.log(student);
      return (<div key={i} style={{border: "red solid 5px"}}>
        <p>Name: {student.name}</p>
        <p>Age: {student.age}</p>
        <p>isMarried: {String(student.isMarried)}</p>
        <button style={{marginRight: "20px"}} onClick={() => {
          navigate(`${student._id}`);
        }}>View</button>
        <button style={{marginRight: "20px"}} onClick={() => {
          navigate(`update/${student._id}`)
        }}>Edit</button>
        <button style={{marginRight: "20px"}} onClick={deleteStudent(student)}>Delete</button>
      </div>)
    })
    return out;
  };

  return (
    <div>
      {displayStudents()}
    </div>
  )
}
