import React from 'react'

const RemoveDataSessionStorage = () => {
    return (
    <div>
        <button onClick={(e) => {
            sessionStorage.removeItem("token");
        }}>
            remove token
        </button>
    </div>
  )
}

export default RemoveDataSessionStorage