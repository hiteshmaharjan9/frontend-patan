import React from 'react'

const AddDataSessionStorage = () => {
  sessionStorage.setItem("token", "123412341234");
  return (
    <div>AddDataSessionStorage</div>
  )
}

export default AddDataSessionStorage

//Session storage is the browser memory of particular url in particular tab(session),
//data will persist in a given session (ie data will be removed when tab closes)