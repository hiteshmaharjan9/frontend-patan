import React from 'react'

const RemoveDataLocalStorage = () => {
  return (
    <div>
      <button onClick={() => {
        localStorage.removeItem("token");
      }}>Remove token</button>
      <button onClick={() => {
        localStorage.removeItem("name")
      }}>Remove name</button>
      <button onClick={() => {
        localStorage.removeItem("age")
      }}>Remove age</button>
      <button onClick={() => {
        localStorage.removeItem("isMarried")
      }}>Remove isMarried</button>
      <button onClick={() => {
        localStorage.clear();
      }}>Remove All</button>
    </div>
  )
}

export default RemoveDataLocalStorage

//Each url in a particular browser has a local storage
//Every web address in a specific web browser has its own local storage

//Local Storage is the browser's memory for a particular url
//The data in a local storage persists even when the session ends (when browser/tab is closed)