import React from 'react'

const AddDataToLocalStorage = () => {

    let token = "123412341234";

    localStorage.setItem("token", token);
    localStorage.setItem("name", "Hitesh");
    localStorage.setItem("age", "23");
    localStorage.setItem("isMarried", "false");

    for (let i = 0; i < 2000; i++)
    {
        localStorage.setItem(`${i}`, `${i}`);
    }
  return (
    <div>AddDataToLocalStorage</div>
  )
}

export default AddDataToLocalStorage