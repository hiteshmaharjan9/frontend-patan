import React from 'react'

const Detail = (props) => {
    // console.log(props);
  return (
    <div>
        My name is {props.name}<br></br>
        Location is {props.address}<br></br>
        Age is {props.age}
        {props.arr}
    </div>
  );
};

export default Detail;