import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

const MyLinks = () => {
  let [render, setRender] = useState(0);
  useEffect(() => {
    setRender(localStorage.getItem("token"));
  }, []);
  return (
    <div>
      <NavLink to="/students" style={{ margin: "20px" }}>
        Students
      </NavLink>
      <NavLink to="/students/create" style={{ margin: "20px" }}>
        Create Students
      </NavLink>
      <NavLink to="/products" style={{ margin: "20px" }}>
        Products
      </NavLink>
      <NavLink to="/products/create" style={{ margin: "20px" }}>
        Create Products
      </NavLink>
      <NavLink to="/admin/register" style={{ margin: "20px" }}>
        Admin Register
      </NavLink>
      <NavLink to="/admin/login" style={{ margin: "20px" }}>
        Admin Login
      </NavLink>
      {render ? (
        <NavLink to="/admin/my-profile" style={{ margin: "20px" }}>
          Admin My Profile
        </NavLink>
      ) : null}
      <NavLink to="/admin/logout" style={{ margin: "20px" }}>
        Logout
      </NavLink>
      <NavLink to="/admin/update-password" style={{ margin: "20px" }}>
        Update Password
      </NavLink>
      <NavLink to="/admin/read-all-user" style={{ margin: "20px" }}>Read All User</NavLink>
    </div>
  );
};

export default MyLinks;
