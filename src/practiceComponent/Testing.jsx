import React from 'react'
import "../global.css"

const Testing = (props) => {
    console.log(props);
  return (
    <div>
        <div className="success">
            {props.val1}
        </div>

        <div className="error">
            error
        </div>

        <div className="info">
            {props.info}
        </div>

        <div className="warning">
            warning
        </div>

        <button className="warning">
            warning
        </button>

        <button className="info">
            info
        </button>
    </div>
  );
};

export default Testing;
