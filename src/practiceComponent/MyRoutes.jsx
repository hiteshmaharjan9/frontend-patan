import React from 'react'
import { Outlet, Route, Routes } from 'react-router-dom'
import CreateProduct from './product/CreateProduct'
import ReadAllProduct from './product/ReadAllProduct'
import CreateStudent from './student/CreateStudent'

const MyRoutes = () => {
  return (
    <div>
        <Routes>

            <Route
            path="/"
            element={<div>This is home page</div>}
            ></Route>

            <Route 
            path="/product/create/:id1" 
            element={<CreateProduct></CreateProduct>}>
            </Route>

            {/* <Route
            path="/product/read"
            element={<ReadAllProduct></ReadAllProduct>}
            ></Route> */}
            
            {/* <Route
              path="/a"
              element={<div>a</div>}
            ></Route>
            
            <Route
              path="/a/a1"
              element={<div>a a1</div>}
            ></Route>

            <Route
              path="/a/:any"
              element={<div>a any</div>}
            ></Route> */}

            <Route
              path="a"
              element={<div>a<Outlet></Outlet></div>}
            >
              <Route 
                path="a1" 
                element={<div>a a1 <Outlet></Outlet></div>}
              >
                <Route
                  path="a2"
                  element={<div>a a1 a2</div>}
                >
                </Route>
              </Route>

              <Route
                path=":any"
                element={<div>a any</div>}
              >
              </Route>
            </Route>

            <Route
              path="product"
              element={<Outlet></Outlet>}>
                <Route
                  path="read"
                  element={<div>/product/read</div>}
                ></Route>
            </Route>

            <Route
            path="/student"
            element={<CreateStudent></CreateStudent>}
            ></Route>

            <Route
            path="/*"
            element={<div>404 Page</div>}
            ></Route>
        </Routes>
    </div>
  )
}

export default MyRoutes