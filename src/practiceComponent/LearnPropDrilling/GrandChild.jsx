import React, { useContext } from "react";
import { Context1, Context2 } from "../../App";

const GrandChild = () => {
  let value = useContext(Context1);
  let value2 = useContext(Context2);
  console.log(value);
  console.log(value2);
  return (
    <div>
      GrandChild name: {value.name}
      <br></br>
      age: {value.age}
      <br></br>
      address: {value2.address}
      <button
        onClick={() => {
          value.setName("hari");
        }}
      >
        change name
      </button>
      <button
        onClick={() => {
          value2.setAddress("India");
        }}
      >
        change address
      </button>
    </div>
  );
};

export default GrandChild;
