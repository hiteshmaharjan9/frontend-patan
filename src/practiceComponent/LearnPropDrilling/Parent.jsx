import React from 'react'
import Child from './Child'

const Parent = () => {
    // console.log(props)
  return (
    <div>
        Parent
        <Child></Child>
    </div>
  )
}

export default Parent