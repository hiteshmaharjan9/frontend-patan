import React from "react";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import MyLinks from "../MyLinks";
import CreateProduct from "../product/CreateProduct";
import ReadAllProduct from "../product/ReadAllProduct";
import ReadSpecificProduct from "../product/ReadSpecificProduct";
import UpdateProduct from "../product/UpdateProduct";
import UpdateSpecificProduct from "../product/UpdateSpecificProduct";
import CreateStudent from "../student/CreateStudent";
import { ReadAllStudent } from "../student/ReadAllStudent";
import ReadSpecificStudent from "../student/ReadSpecificStudent";
import UpdateSpecificStudent from "../student/UpdateSpecificStudent";
import AdminLogin from "../webUsers/AdminLogin";
import AdminProfile from "../webUsers/AdminProfile";
import AdminRegister from "../webUsers/AdminRegister";
import AdminVerify from "../webUsers/AdminVerify";
import AdminLogout from "../webUsers/AdminLogout";
import AdminUpdateProfile from "../webUsers/AdminUpdateProfile";
import AdminUpdatePassword from "../webUsers/AdminUpdatePassword";
import AdminForgotPassword from "../webUsers/AdminForgotPassword";
import AdminResetPassword from "../webUsers/AdminResetPassword";
import ReadAllUser from "../webUsers/ReadAllUser";
import ReadSpecificUser from "../webUsers/ReadSpecificUser";
import UpdateSpecificUser from "../webUsers/UpdateSpecificUser";

const NestingRouteAssignment = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <MyLinks></MyLinks>
              <Outlet></Outlet>
              {/* <div>This is Footer</div> */}
            </div>
          }
        >
          <Route
            path="reset-password"
            element={<AdminResetPassword></AdminResetPassword>}
          ></Route>
          <Route index element={<div>Home Page</div>}></Route>

          <Route path="*" element={<div>404 Page Not Found</div>}></Route>

          <Route
            path="verify-email"
            element={<AdminVerify></AdminVerify>}
          ></Route>

          {/*Products*/}
          <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllProduct></ReadAllProduct>}></Route>

            <Route
              path=":id"
              element={<ReadSpecificProduct></ReadSpecificProduct>}
            ></Route>

            <Route
              path="create"
              element={<CreateProduct></CreateProduct>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route index element={<UpdateProduct></UpdateProduct>}></Route>

              <Route
                path=":id"
                element={<UpdateSpecificProduct></UpdateSpecificProduct>}
              ></Route>
            </Route>
          </Route>

          {/*Students*/}
          <Route
            path="students"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllStudent></ReadAllStudent>}></Route>

            <Route
              path=":id"
              element={<ReadSpecificStudent></ReadSpecificStudent>}
            ></Route>

            <Route
              path="create"
              element={<CreateStudent></CreateStudent>}
            ></Route>

            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                index
                element={
                  <div>
                    <Navigate to="/*"></Navigate>
                  </div>
                }
              ></Route>

              <Route
                path=":id"
                element={<UpdateSpecificStudent></UpdateSpecificStudent>}
              ></Route>
            </Route>
          </Route>

          {/*admin*/}
          <Route
            path="admin"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route
              path="read-all-user"
              element={<ReadAllUser></ReadAllUser>}
            ></Route>

            <Route
              path="update"
              element={<Outlet></Outlet>}
            >

              <Route path=":id" element={<UpdateSpecificUser></UpdateSpecificUser>}></Route>
            </Route>

            <Route
              path="forgot-password"
              element={<AdminForgotPassword></AdminForgotPassword>}
            ></Route>
            <Route
              path="update-password"
              element={<AdminUpdatePassword></AdminUpdatePassword>}
            ></Route>
            <Route
              path="update-profile"
              element={<AdminUpdateProfile></AdminUpdateProfile>}
            ></Route>

            <Route path="logout" element={<AdminLogout></AdminLogout>}></Route>
            <Route
              path="my-profile"
              element={<AdminProfile></AdminProfile>}
            ></Route>
            <Route
              path=":id"
              element={<ReadSpecificUser></ReadSpecificUser>}
            ></Route>

            <Route
              index
              element={
                <div>
                  This is the Admin dashboard<br></br>
                </div>
              }
            ></Route>

            <Route
              path="register"
              element={<AdminRegister></AdminRegister>}
            ></Route>

            <Route path="login" element={<AdminLogin></AdminLogin>}></Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default NestingRouteAssignment;
