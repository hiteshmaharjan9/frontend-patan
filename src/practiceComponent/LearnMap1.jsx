import React from 'react'

const LearnMap1 = () => {
    let name = ["nitan", "ram", "hari"];
    let printName = () => {
      let list = name.map((value, i) => {
        return (<div>My best friend is {value}</div>);
      });
      return list;
    };
  return (
    <div>
        {printName()}
    </div>
  )
};

export default LearnMap1;