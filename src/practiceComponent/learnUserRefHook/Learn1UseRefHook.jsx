import React, { useRef } from 'react'

const Learn1UseRefHook = () => {
    let ref1 = useRef();
    let ref2 = useRef();
    let refInput1 = useRef();
  return (
    <div>
        <button onClick = {(e) => {
            let a  = ref1.current.style;
            a.fontSize = "50px";
            if ( a.backgroundColor === "red")
            {
                a.backgroundColor = "white"
            }
            else
                a.backgroundColor = "red"
        }}>Color Red</button>
        <button onClick = {(e) => {
            if ( ref2.current.style.backgroundColor === "blue")
            {
                ref2.current.style.backgroundColor = "white"
            }
            else
                ref2.current.style.backgroundColor = "blue"
        }}>Color Blue</button>
        <br></br>
        <button onClick={(e) => {
            refInput1.current.focus();
        }}>Focus</button>
        <button onClick={(e) => {
            refInput1.current.blur();
        }}>Blur</button>
        <p ref={ref1}>Red</p>
        <p ref={ref2}>Blue</p>
        <input ref={refInput1}></input>

    </div>
  )
}

export default Learn1UseRefHook