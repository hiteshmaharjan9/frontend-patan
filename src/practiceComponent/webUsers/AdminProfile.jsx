import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const AdminProfile = () => {
  let [profile, setProfile] = useState({});
  let navigate = useNavigate();
  let viewProfile = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/my-profile`,
        method: `get`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      console.log(result.data.result);
      setProfile(result.data.result);
      // console.log(profile);
    } catch (error) {
      console.log(error.response.data);
    }
  };
  // viewProfile();
  useEffect(() => {
    viewProfile();
  }, []);
  console.log(profile);
  return (
    <div> 
      <p>Name: {profile.fullName}</p>
      <p>Email: {profile.email}</p>
      <p>Date of Birth: {new Date(profile.dob).toLocaleDateString()}</p>
      <p>Gender: {profile.gender}</p>
      <p>Role: {profile.role}</p>
      <button onClick={() => {
        navigate("/admin/update-profile");
      }}>Update profile</button>
    </div>
  );
};

export default AdminProfile;
