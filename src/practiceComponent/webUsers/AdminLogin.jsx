import axios from "axios";
import React, { useState } from "react";
import { ToastContainer } from "react-toastify";
import { displayError, displaySuccess } from "../../toast";
import { useNavigate } from "react-router-dom";

const AdminLogin = () => {
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let navigate = useNavigate();

  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
      email: email,
      password: password,
    };
    console.log(data);

    try {
      let result = await axios({
        url: "http://localhost:8000/web-users/login",
        method: "post",
        data: data,
      });
      console.log(result);
      // console.log(result.data.token)
      localStorage.setItem("token", result.data.token);

      navigate(`/admin`);
    } catch (error) {
      console.log(error);
      displayError(error.response.data.message);
    }
  };
  return (
    <>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        {/* in input whatever you place in value is displayed in the input in the browser */}
        {/*email*/}
        <div>
          <label htmlFor="email">email: </label>
          <input
            type="email"
            placeholder="hitesh@gmail.com"
            id="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
              console.log(e.target.value);
            }}
          ></input>
        </div>
        {/*password*/}
        <div>
          <label htmlFor="password">Password: </label>
          <input
            type="password"
            id="password"
            placeholder="your password*"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
              console.log(e.target.value);
            }}
          ></input>
        </div>

        <br></br>
        <button type="submit">Login</button>
        <div
          style={{ cursor: "pointer" }}
          onClick={() => {
            navigate("/admin/forgot-password");
          }}
        >
          forgot password?
        </div>
      </form>
    </>
  );
};

export default AdminLogin;

/*
    make form
    hit api (token)
    save token to local storage
    navigate to /admin
*/
