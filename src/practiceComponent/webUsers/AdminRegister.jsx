import React, { useState } from "react";
import { genders } from "../Form/Data";
import axios from "axios";
import { displayError, displaySuccess } from "../../toast";
import { ToastContainer } from "react-toastify";

const AdminRegister = () => {
  let [fullName, setfullName] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [dob, setDob] = useState("");
  let [gender, setGender] = useState("female");

  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
      fullName: fullName,
      email: email,
      password: password,
      dob: dob,
      gender: gender
    };

    data = {
      ...data,
      role: "admin"
    };

    try {
      let result = await axios({
        url: "http://localhost:8000/web-users",
        method: "post",
        data: data
      });
      console.log(result);
      displaySuccess("A link has been sent to your email. Please click it to verify.");
      setfullName("");
      setEmail("");
      setPassword("");
      setDob("");
      setGender("female");
      // displaySuccess()
    } catch (error) {
      console.log(error);
      displayError(error.response.data.message);
    }
  };
  let optionGenders = genders.map((gen, i) => {
    return (
      <div key={i}>
        <label htmlFor={gen.value}>{gen.label}</label>
        <input
          type="radio"
          id={gen.value}
          value={gen.value}
          checked={gender === gen.value}
          onChange={(e) => {
            setGender(e.target.value);
            console.log(e.target.value);
          }}
        ></input>
      </div>
    );
  });
  return (
    <>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="fullName">Full Name: </label>
          <input
            type="text"
            placeholder="Hitesh Maharjan"
            id="fullName"
            value={fullName}
            onChange={(e) => {
              console.log("changed");
              setfullName(e.target.value);
              console.log(e.target.value);
            }}
          ></input>
        </div>
        {/* in input whatever you place in value is displayed in the input in the browser */}
        {/*email*/}
        <div>
          <label htmlFor="email">email: </label>
          <input
            type="email"
            placeholder="hitesh@gmail.com"
            id="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
              console.log(e.target.value);
            }}
          ></input>
        </div>
        {/*password*/}
        <div>
          <label htmlFor="password">Password: </label>
          <input
            type="password"
            id="password"
            placeholder="your password*"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
              console.log(e.target.value);
            }}
          ></input>
        </div>

        <div>
          <label htmlFor="dob">Date of Birth</label>
          <input
            id="dob"
            type="date"
            value={dob}
            onChange={(e) => {
              setDob(e.target.value);
              console.log(e.target.value);
            }}
          ></input>
        </div>
        <div>
          {optionGenders}
        </div>

        <br></br>
        <button type="submit">Proceed</button>
      </form>
    </>
  );
};

export default AdminRegister;
