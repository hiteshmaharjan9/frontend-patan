import axios from "axios";
import React, { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { displayError, displaySuccess } from "../../toast";
import { ToastContainer } from "react-toastify";

// const AdminVerify = () => {
//   let [query] = useSearchParams();
//   let token = query.get("token");
//   let navigate = useNavigate();

//   let verify = async () => {
//     try {
//       let result = await axios({
//         url: "http://localhost:8000/web-users/verify-email",
//         method: "patch",
//         headers: {
//           Authorization: `Bearer ${token}`,
//         },
//       });
//       console.log(result);
//       displaySuccess(result.data.message);
//     } catch (error) {
//       console.log(error);
//       displayError(error.response.data.message);
//     }
//   };
//   verify();
//   return (
//     <div>
//       <ToastContainer></ToastContainer>
//       AdminVerify
//       {/* {navigate(`/login`)}; */}
//     </div>
//   );
// };

// export default AdminVerify;

const AdminVerify = () => {
  let [query] = useSearchParams();
  let token = query.get("token");
  let navigate = useNavigate();
  let verifyEmail = async() => {
    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/verify-email`,
        method: `patch`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      navigate(`/admin/login`);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    verifyEmail();
  }, []);

  return <div>verifyEmail</div>;
};

export default AdminVerify;
