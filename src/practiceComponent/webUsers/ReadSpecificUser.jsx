import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const ReadSpecificUser = () => {
    let [user, setUser] = useState({});
    let params = useParams();
    let readUser = async() => {
        let result = await axios({
            url: `http://localhost:8000/web-users/${params.id}`,
            method: `get`,
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        });
        // console.log(result.data.result);
        setUser(result.data.result);
    };
    useEffect(() => {
        readUser();
    }, [])
  return (
    <div>
        <p>Full Name: {user.fullName}</p>
        <p>Email: {user.email}</p>
        <p>Date of Birth: {(new Date(user.dob)).toLocaleDateString()}</p>
        <p>Gender: {user.gender}</p>
        <p>Role: {user.role}</p>
        <p>isVerified: {String(user.isVerifiedEmail)}</p>
    </div>
  )
}

export default ReadSpecificUser