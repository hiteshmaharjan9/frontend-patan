import { Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { genders } from "../Form/Data";
import axios from "axios";
import { useNavigate } from "react-router-dom";

const AdminUpdateProfile = () => {
  let [fullName, setFullName] = useState("");
  let [dob, setDob] = useState("");
  let [gender, setGender] = useState("");

  let navigate = useNavigate();
  let onSubmit = async (value, others) => {
    console.log(value);
    let result = await axios({
      url: `http://localhost:8000/web-users/update-profile`,
      method: `patch`,
      data: value,
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    // navigate("/admin/my-profile");
  };

  let getInfo = async () => {
    let result = await axios({
      url: `http://localhost:8000/web-users/my-profile`,
      method: `get`,
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });
    let info = result.data.result;
    // fullName = info.fullName;
    // dob = info.dob;
    // gender = info.gender;
    setFullName(info.fullName);
    let [month, day, year] = info.dob
    setDob(new Date(info.dob).toLocaleDateString());
    // console.log(info.dob)
    console.log(new Date(info.dob).toLocaleDateString());

    setGender(info.gender);
  };

  useEffect(() => {
    getInfo();
  }, []);

  return (
    <Formik
      initialValues={{
        fullName: fullName,
        dob: dob,
        gender: gender,
      }}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {(formik) => {
        return (
          <Form>
            <Field name="fullName">
              {({ field, form, meta }) => {
                console.log(meta.value);
                return (
                  <>
                    <label htmlFor="fullName">Name: </label>
                    <input
                      id="fullName"
                      type="text"
                      placeholder="John Doe"
                      value={meta.value}
                      onChange={(e) => {
                        formik.setFieldValue("fullName", e.target.value);
                      }}
                    ></input>
                  </>
                );
              }}
            </Field>
            <br></br>
            <Field name="dob">
              {({ field, form, meta }) => {
                console.log(meta.value);
                return (
                  <>
                    <label htmlFor="dob">Date of Birth: </label>
                    <input
                      id="dob"
                      type="date"
                      value={meta.value}
                      onChange={(e) => {
                        formik.setFieldValue("dob", e.target.value);
                      }}
                    ></input>
                  </>
                );
              }}
            </Field>
            <Field name="gender">
              {({ field, form, meta }) => {
                console.log(meta.value);
                return (
                  <>
                    {genders.map((gen, i) => {
                      return (
                        <div key={i}>
                          <label htmlFor={gen.value}>{gen.label}</label>
                          <input
                            type="radio"
                            id={gen.value}
                            value={gen.value}
                            checked={meta.value === gen.value}
                            onChange={(e) => {
                              formik.setFieldValue("gender", e.target.value);
                            }}
                          ></input>
                        </div>
                      );
                    })}
                  </>
                );
              }}
            </Field>
            <button type="submit">Update</button>
          </Form>
        );
      }}
    </Formik>
  );
};

export default AdminUpdateProfile;
