import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ReadAllUser = () => {
  let [users, setUsers] = useState([]);
  let navigate = useNavigate();
  let getAllUsers = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/web-users`,
        method: `GET`,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      //   console.log(result);
      // console.log(result.data.result);
      setUsers(result.data.result);
    } catch (error) {
      console.log(error.message);
    }
  };

  let deleteUser = (user) => {
    return () => {
      let _deleteUser = async () => {
        try {
          let result = await axios({
            url: `http://localhost:8000/web-users/${user._id}`,
            method: `delete`,
            headers: {
              Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
          });
          console.log(result);
          getAllUsers();
        } catch (error) {
          console.log(error.message);
        }
      };
      _deleteUser();
    };
  };

  useEffect(() => {
    getAllUsers();
  }, []);

  console.log(users);

  let usersInfo = users.map((user, i) => (
    <div key={i} style={{ border: "solid red 2px", borderBottom: "20px" }}>
      {/* {console.log(user)} */}
      <p>Full Name: {user.fullName}</p>
      <p>Email: {user.email}</p>
      <p>Date of Birth: {new Date(user.dob).toLocaleDateString()}</p>
      <p>Role: {user.role}</p>
      <button
        style={{ marginRight: "20px" }}
        onClick={() => {
          navigate(`/admin/${user._id}`);
        }}
      >
        View
      </button>
      <button
        style={{ marginRight: "20px" }}
        onClick={(e) => {
          navigate(`/admin/update/${user._id}`);
        }}
      >
        Edit
      </button>
      <button style={{ marginRight: "20px" }} onClick={deleteUser(user)}>
        Delete
      </button>
    </div>
  ));
  return <div>{usersInfo};</div>;
};

export default ReadAllUser;

//invalidation
