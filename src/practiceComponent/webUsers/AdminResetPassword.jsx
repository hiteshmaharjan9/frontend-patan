import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom';
import { displayError, displaySuccess } from '../../toast';
import { ToastContainer } from 'react-toastify';

const AdminResetPassword = () => {
    let [newPassword, setNewPassword] = useState("");
    let navigate = useNavigate();
    let [query] = useSearchParams();
    let token = query.get("token");

    let onSubmit = async(e) => {
        e.preventDefault();
        let data = {
            password: newPassword
        }
        try {
            let result = await axios({
                url: `http://localhost:8000/web-users/reset-password`,
                method: `patch`,
                data: data,
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            console.log(result);
            // displaySuccess(result.data.message);
            navigate("/admin/login");
        } catch (error) {
            console.log(error.response.data.message);
            displayError(error.response.data.message);
        }
    }
  return (
        <>
            <ToastContainer></ToastContainer>
            <form onSubmit={onSubmit}>
                <div>
                    <label htmlFor="newPassword">New Password: </label>
                    <input id="newPassword" type="password" value={newPassword} onChange={(e)=> {setNewPassword(e.target.value)}}></input>
                </div>
                <button>submit</button>


            </form>
        </>
    )
}

export default AdminResetPassword