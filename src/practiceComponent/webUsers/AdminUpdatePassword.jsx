import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { displayError } from "../../toast";
import { ToastContainer } from "react-toastify";

const AdminUpdatePassword = () => {
  let [oldPassword, setOldPassword] = useState("");
  let [newPassword, setNewPassword] = useState("");
  let navigate = useNavigate();

  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
      oldPassword: oldPassword,
      newPassword: newPassword,
    };
    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/update-password`,
        method: `patch`,
        data: data,
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      console.log(result);
      localStorage.removeItem("token");
      navigate(`/admin/login`);
    } catch (error) {
      console.log(error);
      displayError(error.response.data.message)
    }
  };

  return (
    <>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="oldPassword">Old Password: </label>
          <input
            type="password"
            id="oldPassword"
            value={oldPassword}
            onChange={(e) => {
              setOldPassword(e.target.value);
            }}
          ></input>
        </div>
        <div>
          <label htmlFor="newPassword">New Password: </label>
          <input
            type="password"
            id="newPassword"
            value={newPassword}
            onChange={(e) => {
              setNewPassword(e.target.value);
            }}
          ></input>
        </div>
        <button type="submit">update password</button>
      </form>
    </>
  );
};

export default AdminUpdatePassword;
