import axios from 'axios';
import React, { useState } from 'react'
import { displaySuccess } from '../../toast';
import { ToastContainer } from 'react-toastify';

const AdminForgotPassword = () => {
    let [email, setEmail] = useState("");
    let onSubmit = async (e) => {
        e.preventDefault();
        let data = {
            email: email
        };
        try {
            let result = await axios({
                url:`http://localhost:8000/web-users/forgot-password`,
                method: `post`,
                data: data
            });
            setEmail("");
            console.log(result);
            displaySuccess(result.data.message);
        } catch (error) {
            console.log(error);
        }
    }
  return (
    <>
        <ToastContainer></ToastContainer>
        <form onSubmit={onSubmit}>
            <div>
                <label htmlFor="email">Email: </label>
                <input type="email" id="email" value={email} onChange={(e) => {setEmail(e.target.value)}}></input>
            </div>
            <button type="submit">Proceed</button>
        </form>
    </>
  )
}

export default AdminForgotPassword