import React, { useState } from 'react'

const Toggle = () => {
    let [image, setImage] = useState(true);
    // let [text, setText] = useState("show");
    // let handleImage = (x) => {
    //     return (e) => {
    //         setImage(x)
    //     };
    // };

    let handleImage = (e) => {
        if (image === true){
            
            // setText("show");
            setImage(false);
        }
        else
        {
            // setText("hide")
            setImage(true);

        }
    };
  return (
    <div>
        {image?<img src="./logo192.png"></img>:null}
        {/* <button onClick={image?handleImage(false):handleImage(true)}>button</button> */}
        <br></br>
        <button onClick={handleImage}>
            {image === true? "hide":"show"}
            </button>

    </div>
  )
}

export default Toggle