import React, { useState } from 'react'

const LearnUseStateHook2 = () => {
    
    let [count, setCount] = useState(0);
    
    let increment = (e) => {
        if (count < 10)
        {
            setCount(count + 1);
            console.log(count);
        }
    };

    let decrement = (e) => {
        if (count > 0)
        {
            setCount(count - 1);
            console.log(count);
        }
    };

    let reset = (e) => {
      setCount(0);
      console.log(count);
    };
    return (
    <div>
        Count = {count}
        <br></br><button onClick={increment}>+1</button>
        
        <br></br><button onClick={decrement}>-1</button>
        <br></br><button onClick={reset}>reset</button>

    </div>
  );
};

export default LearnUseStateHook2;