import React, { useState } from 'react'

const WhyUseState = () => {
    let [name, setName] = useState("hitesh");
    console.log("...");


    return (
    <div>
        {name}
        <button onClick={(e) => setName("ram")}>Change Name</button>
    </div>
  )
}

export default WhyUseState;