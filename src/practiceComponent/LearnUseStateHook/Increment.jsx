import React, { useState } from 'react'

const Increment = () => {
    let [count, setCount] = useState(0);
    let [count2, setCount2] = useState(100);
    let inc = (e) => {
        setCount(count + 1)
    };
    let inc2 = (e) => {
        setCount2(count2 + 1);
    };
  return (
    <div>
        Count = {count}
        <br></br>
        Count2 = {count2}
        <br></br>
        <button onClick={inc}>Increment</button>
        <br></br>
        <button onClick={inc2}>Increment2</button>
    </div>
  )
}

export default Increment