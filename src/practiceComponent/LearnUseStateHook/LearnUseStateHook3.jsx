import React, { useState } from 'react'

const LearnUseStateHook3 = () => {

    let [showImg, setShowImg] = useState(false);

    let showImgFun = (e) => {
        setShowImg(true);
    };

    let hideImgFun = (e) => {
        setShowImg(false);
    };

    return (
        <div>
            {
                showImg?<img src="./logo192.png" alt="logo"></img>:null
            }
            <br></br>
            <button onClick={showImgFun}>Show Image</button><br></br>
            <button onClick={hideImgFun}>Hide Image</button>
        </div>
    )
}

export default LearnUseStateHook3