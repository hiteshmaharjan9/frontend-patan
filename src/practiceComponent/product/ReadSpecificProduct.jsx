import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'

const ReadSpecificProduct = () => {
  let param = useParams();
  let [product, setProduct] = useState({});
  // console.log(param);

  let sendRequest = async () => {    
    try {
      let result = await axios({
        url: `http://localhost:8000/products/${param.id}`,
        method: "get"
      });
      setProduct(result.data.result);
      // console.log(product);
    } 
    catch (error) {
      console.log(error);          
    }
  };
  
  useEffect(() => {
    sendRequest();
  }, []);

  console.log(product);
  let displayProduct = () => {
    return (<div>
      <p>Product: {product.name}</p>
      <p>Price: {product.price}</p>
      <p>Quantity: {product.quantity}</p>
    </div>)
  }
  return (
    <div>
      {displayProduct()}
    </div>
  )
}

export default ReadSpecificProduct