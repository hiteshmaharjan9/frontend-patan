
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const UpdateSpecificProduct = () => {
  let navigate = useNavigate();
  let params = useParams();
  let [oldData, setOldData] = useState({});
  console.log(oldData);
  let [name, setName] = useState(oldData.name);
  let [price, setPrice] = useState(oldData.price);
  let [quantity, setQuantity] = useState(oldData.quantity);
  console.log("name => ",name)

  // let [name, setName] = useState("");
  // let [price, setPrice] = useState("");
  // let [quantity, setQuantity] = useState("");

  let getOldData = async () => {
    let result = await axios({
      url: `http://localhost:8000/products/${params.id}`,
      method: `get`
    })
    // console.log(result);
    // oldData = oldData.data.result;
    setOldData(result.data.result);
    // console.log(oldData);
    
  }
  // console.log(oldData);



  let getProduct = async () => {
    let result = await axios({
      url: `http://localhost:8000/products/${params.id}`,
      method: `get`
    })
    let oldData = result.data.result;
    setName(oldData.name);
    setPrice(oldData.price);
    setQuantity(oldData.quantity);
  };

  useEffect(() => {
    // getProduct();
    getOldData();
  }, []);

  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
      name : name,
      price: price,
      quantity: quantity
    };
    // console.log(data);

    try { 
      let result = await axios({
        url: `http://localhost:8000/products/${params.id}`,
        method: "patch",
        data: data
      });
      console.log(result);
      setName("");
      setPrice("");
      setQuantity("");
      toast.success(result.data.message, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
      });
      navigate(`/products/${params.id}`);
    } 
    catch (error) {
      console.log(error);
      toast.error(error.response.data.message, {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });
      }
  }

  return (
    <div>
      {/* <ToastContainer position="bottom-right"/> */}
      <ToastContainer/>
      <form onSubmit={onSubmit}>
        <label htmlFor="name">Name:</label>
        <input 
          type="text" 
          id="name" 
          placeholder="name" 
          value={name} 
          onChange={
            (e) => {
              setName(e.target.value)
              }
            }></input>
        <br></br>
        <label htmlFor="price">Price: </label>
        <input 
          type="number" 
          id="price" 
          placeholder="price" 
          value={price} 
          onChange={
            (e) => {
              setPrice(e.target.value)
              }
            }></input>
        <br></br>
        <label htmlFor="quantity">Quantity: </label>
        <input 
          type="number" 
          id="quantity" 
          placeholder="quantity" 
          value={quantity} 
          onChange={
            (e) => {
              setQuantity(e.target.value)
            }
          }></input>
        <br></br>
        <button type="submit">Update</button>
      </form>
    </div>
  )
}

export default UpdateSpecificProduct;