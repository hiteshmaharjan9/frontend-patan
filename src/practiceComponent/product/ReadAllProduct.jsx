import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';

const ReadAllProduct = () => {
  let [products, setProducts] = useState([]);
  let navigate = useNavigate();
  let getAllProducts = async() => {
    try {
          let result = await axios({
            url: `http://localhost:8000/products`,
            method: `GET`
          });
          // console.log(result);
          // console.log(result.data.result);
          setProducts(result.data.result);
    } catch (error) {
      console.log(error.message);
    }
  };

  let deleteProduct = (product) => {
    return () => {
      let _deleteProduct = async () => {
      try {
              let result = await axios({
                url: `http://localhost:8000/products/${product._id}`,
                method: `delete`
              });
              console.log(result);
              getAllProducts();
      } catch (error) {
        console.log(error.message);
      }
    }
    _deleteProduct();
    }
  };

  useEffect(() => {
    getAllProducts();
  }, []);

  console.log(products);

  let productsInfo = products.map((product, i) =>
  <div key={i} style={{border: "solid red 2px", borderBottom: "20px"}}>
    {/* {console.log(product)} */}
  <p>Product: {product.name}</p>
  <p>Price: {product.price}</p>
  <p>Quantity: {product.quantity}</p>
  <button 
    style={{marginRight:"20px"}} 
    onClick={() => {
      navigate(`/products/${product._id}`);
  }}>View</button>
  <button style={{marginRight:"20px"}} onClick={(e) => {
    navigate(`update/${product._id}`);
  } }>Edit</button>
  <button style={{marginRight:"20px"}} onClick={deleteProduct(product)}>Delete</button>
</div>
)
  return (
    <div>
      {productsInfo};
    </div>
  )
}

export default ReadAllProduct

//invalidation